package com.ruyuan.little.project.elasticsearch.biz.admin.service;

import com.ruyuan.little.project.common.dto.CommonResponse;
import com.ruyuan.little.project.elasticsearch.biz.admin.dto.AdminGoodsSpuDTO;
import com.ruyuan.little.project.elasticsearch.biz.admin.entity.AdminGoodsSku;
import com.ruyuan.little.project.elasticsearch.biz.admin.entity.AdminGoodsSpu;

import java.util.List;

/**
 * @author dulante
 * version: 1.0
 * Description:后台商品service组件
 **/
public interface AdminGoodsService {

    /**
     * 查询商品列表
     *
     * @param adminGoodsSpuDTO 后台商品查询条件
     * @return 结果
     */
    CommonResponse list(AdminGoodsSpuDTO adminGoodsSpuDTO);

    /**
     * 添加商品spu
     *
     * @param adminGoodsSpu 商品spu信息
     * @param parentId      店铺id
     * @return 结果
     */
    CommonResponse insertGoodsSpu(AdminGoodsSpu adminGoodsSpu, String parentId);

    /**
     * 添加商品sku
     *
     * @param adminGoodsSpu     商品spu
     * @param adminGoodsSkuList 商品sku列表
     * @return 结果
     */
    CommonResponse insertGoodsSku(AdminGoodsSpu adminGoodsSpu, List<AdminGoodsSku> adminGoodsSkuList);

    /**
     * 修改商品spu
     *
     * @param adminGoodsSpu 商品spu
     * @return 结果
     */
    CommonResponse updateGoodsSpu(AdminGoodsSpu adminGoodsSpu);

    /**
     * 修改商品sku
     *
     * @param adminGoodsSku 商品sku
     * @return 结果
     */
    CommonResponse updateGoodsSku(AdminGoodsSku adminGoodsSku);

    /**
     * 根据id查询商品Spu详情
     *
     * @param id 商品spuId
     * @return 结果
     */
    CommonResponse getSpuDetailById(String id);


    /**
     * 根据id删除商品
     *
     * @param id id
     * @return 结果
     */
    CommonResponse deleteGoodsById(String id);

    /**
     * 自动上架指定时间之前的商品
     *
     * @param onlineTime 上架时间
     */
    void autoOnlineGoodsSpuByOnlineTime(String onlineTime);

    /**
     * 使用Scroll自动上架指定时间之前的商品
     *
     * @param onlineTime 上架时间
     */
    void autoOnlineGoodsSpuByOnlineTimeScroll(String onlineTime);

    /**
     * 重建索引
     *
     * @param indexAlias    索引别名
     * @param oldIndex      旧索引
     * @param newIndex      新索引
     * @param resourceName  新索引mapping文件名称
     * @return 结果
     */
    CommonResponse reindex(String indexAlias, String oldIndex, String newIndex, String resourceName);
}
